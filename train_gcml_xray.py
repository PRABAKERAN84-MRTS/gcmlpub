import torch
import torch.nn as nn
import torchvision
#import torchvision.transforms as transforms
from torchvision import datasets, models, transforms
import torch.nn.functional as F
import matplotlib.pyplot as plt
import numpy as np
import time
import os
import copy
import csv
import metrx as mx

DATA_DIR = 'data/covid'
LOG_DIR = 'logs'
# progress file name will be written to the logs dir
BATCH_SIZE = 12
WORKERS = 12
# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Hyper-parameters
num_epochs = 15
learning_rate = 0.001
#cam_activation_point = 0.499
#cam_activation_point = 0.549
cam_activation_point = 0.05
# this depends on the final resolution map size of the CNN, and is just h*w of it.
gcl_length = 25

IMAGE_NET_MEAN = [0.485, 0.456, 0.406]
IMAGE_NET_STD = [0.229, 0.224, 0.225]

# data setup
# data processing, adding additional sets and transforms, i.e. test corresponds to a directory
# and will create appropriate datasets
data_transforms = {
    'train': transforms.Compose([
        transforms.RandomResizedCrop(224),
        #transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(IMAGE_NET_MEAN, IMAGE_NET_STD)

    ]),
    'val': transforms.Compose([
        transforms.RandomResizedCrop(224),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(IMAGE_NET_MEAN, IMAGE_NET_STD)
    ])
}

# use the ImageFolder class from torchvision that accepts datasets structured by datax.py
# create datasets with appropriate transformation
image_datasets = {x: datasets.ImageFolder(os.path.join(DATA_DIR, x), data_transforms[x]) for x in data_transforms}
# create dataloaders
dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=BATCH_SIZE, shuffle=True, num_workers=WORKERS) for x in data_transforms}

dataset_sizes = {x: len(image_datasets[x]) for x in data_transforms}
class_names = image_datasets['train'].classes
number_classes = len(class_names)





class Hook():
    def __init__(self, module, backward=False):
        if backward==False:
            self.hook = module.register_forward_hook(self.hook_fn)
        else:
            self.hook = module.register_backward_hook(self.hook_fn)
    def hook_fn(self, module, input, output):
        self.input = input
        self.output = output
    def close(self):
        self.hook.remove()

def compute_cams(activations, labels, weights):
    # get number of batches
    cams = []
    gcl_index = []
    batches,_,_,_ = activations.size()
    for b in range(batches):
        cw = weights[labels[b],:] # get class weights for label[b]
        a = activations[b,:]
        # dot product between class weights vector and filter activations
        cam = (cw.view(-1,1,1) * a).sum(0)
        cam = F.relu(cam)
        # normalize
        cam_min, cam_max = cam.min(), cam.max()
        cam = (cam - cam_min).div(cam_max - cam_min)
        cams.append(cam)
        # print('CAM:\n',cam)
        # n,m = cam.size()
        index = cam_to_index(cam.view(-1), gcl_length)
        gcl_index.append(index.item())
        #print("Index: ", index.item())

    return cams, gcl_index


# takes a 2D tensor and computes an index for global correlation vector,
# which is a powerset of all positive CAM activations
def cam_to_index(cam, bits):
    # cam_activation_point is the threshhold on cam activations
    a = torch.where(cam > cam_activation_point, 1, 0)
    b = a.view(-1)
    mask = 2 ** torch.arange(bits - 1, -1, -1).to(b.device, b.dtype)
    return torch.sum(mask * b, -1)



if __name__ == '__main__':

    # create the GCL structure, we use ones for Laplace smoothing
    gcl = torch.ones(number_classes, (1 << gcl_length))
    gcl = gcl.to(device)
    model = torch.load('models/resGCML.pt')
    model.eval()
    model = model.to(device)
    #print(model)
    hook = Hook(model.relu2)
    with torch.no_grad():
        for epoch in range(num_epochs):
        
            for i, (images, labels) in enumerate(dataloaders['train']):
                images = images.to(device)
                labels = labels.to(device)
                outputs = model(images)
                # print("HOOK ACTIVATIONS: ", hook.output.size())
                # print("Labels: ", labels)
                cams, gcls = compute_cams(hook.output, labels, model.fc.weight)
                #print("GCL: ", gcls)

                for (label, index) in zip(labels, gcls):
                    gcl[label][index] += 1.0
            print("Epoch T complete: ",epoch)
            # run through the validation dataset as well
            for i, (images, labels) in enumerate(dataloaders['val']):
                images = images.to(device)
                labels = labels.to(device)
                outputs = model(images)
                # print("HOOK ACTIVATIONS: ", hook.output.size())
                # print("Labels: ", labels)
                cams, gcls = compute_cams(hook.output, labels, model.fc.weight)
                #print("GCL: ", gcls)

                for (label, index) in zip(labels, gcls):
                    gcl[label][index] += 1.0
            # end val gcml training
            print("Epoch V complete: ",epoch)
            
        # persit our gcl file
        torch.save(gcl,'gcml/gclxray05_val15.pt')
    # let's look at the weigths tensor for class 3

    #print("Weights at fc:\n", model.fc.weight[3,:].size())